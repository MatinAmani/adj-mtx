class Node:
    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next


class LinkedList:
    def __init__(self, list=None):
        self.head = None
        if list:
            for item in list:
                self.add_to_end(item)

    def add_to_beg(self, data):
        self.head = Node(data, self.head)

    def add_to_end(self, data):
        if self.head is None:
            self.head = Node(data)
        else:
            ptr = self.head
            while ptr.next:
                ptr = ptr.next
            ptr.next = Node(data)

    def print(self):
        if self.head is None:
            print('linked list is empty')
            return

        ptr = self.head
        while ptr:
            print(ptr.data, end=' -> ')
            ptr = ptr.next
        print('None')

    def length(self):
        len = 0
        ptr = self.head
        while ptr:
            len += 1
            ptr = ptr.next
        return len
