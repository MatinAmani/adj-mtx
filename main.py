from graph import Graph

nodes = ['a', 'b', 'c', 'd', 'e']
adj = [
    [0, 0, 0, 0, 0],
    [2, 0, 1, 0, 1],
    [0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0],
    [0, 0, 1, 1, 0]
]


graph = Graph(nodes, adj)

graph.print_adj_list()
print(graph.inc_matrix)
