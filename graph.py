import pandas as pd
import numpy as np
from linked_list import LinkedList


class Graph:
    def __init__(self, nodes, adj_mtx):
        adj_mtx = np.array(adj_mtx)
        self.nodes = nodes
        self.adj_list = None
        if self.check_adj(adj_mtx):
            self.matrix = pd.DataFrame(
                data=adj_mtx, index=nodes, columns=nodes)
            self.calc_adj_list()
            self.calc_inc_matrix()
        else:
            print('Adjacency matrix not valid! check the given matrix and try again.')

    def check_adj(self, adj_mtx):
        if len(adj_mtx.shape) == 2:
            if adj_mtx.shape[0] == adj_mtx.shape[1]:
                return True
        return False

    def calc_adj_list(self):
        self.adj_list = {node: LinkedList([node]) for node in self.nodes}
        for node in self.nodes:
            for i in range(len(self.nodes)):
                if self.matrix.loc[node][i] > 0:
                    self.adj_list[node].add_to_end(self.nodes[i])

    def calc_inc_matrix(self):
        self.inc_matrix = pd.DataFrame(index=self.nodes)
        count = 0
        for node in self.nodes:
            for j in range(len(self.nodes)):
                if self.matrix.loc[node][j] > 0:
                    data = np.zeros(len(self.nodes), dtype=int)
                    data[self.nodes.index(node)] = -self.matrix.loc[node][j]
                    data[j] = self.matrix.loc[node][j]
                    self.inc_matrix[count] = data
                    count += 1

    def print_adj_list(self):
        if self.adj_list:
            for node in self.nodes:
                self.adj_list[node].print()
        else:
            print('No adjacency matrix found!')

    def is_bin_tree(self):
        for i in range(len(self.nodes)):
            count = 0
            for j in range(i, len(self.nodes)):
                count = count + 1 if self.matrix.iloc[i][j] == 1 else count
            if count > 2:
                return False
        return True
